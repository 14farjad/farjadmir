#!/usr/bin/env python
# coding: utf-8

# In[3]:


import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[4]:


import os
print (os.getcwd())


# In[5]:


data = pd.read_csv('fakeData.csv')


# In[6]:


data.head()


# In[7]:


sns.jointplot('Date', 'ProductQuantity', data=data)


# In[8]:


sns.heatmap(data.corr())


# In[9]:


data.isnull().sum()


# In[10]:


X= data[['Date', 'ProductName']]


# In[11]:


y= data['ProductQuantity']


# In[12]:


print(X)


# In[13]:


print(y)


# In[14]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y)


# In[15]:


from sklearn.linear_model import LinearRegression


# In[16]:


logModel = LinearRegression() 


# In[17]:


logModel.fit(X_train, y_train)


# In[18]:


y_pred=logModel.predict(X_test)


# In[19]:


print(y_pred)


# In[20]:


from sklearn.metrics import classification_report


# In[21]:


print(classification_report(y_test, y_pred))


# In[22]:


score = logModel.score(X_test, y_test)
print(score)


# In[23]:


from sklearn import metrics


# In[24]:


cm = metrics.confusion_matrix( y_test, y_pred )
print(cm)


# In[51]:


X_new = [[20, 105]]


# In[52]:


y_new = logModel.predict(X_new)
print("X=%s, Predicted=%s" % (X_new[0], y_new[0]))


# In[ ]:




